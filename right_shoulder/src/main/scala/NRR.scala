object NRR extends App {

    // Code to wrap a scan using an existing statistical shape model
    // NOTE: To run code blocks:
    // - RMB click -> Scala REPL
    // - Select code block with mouse i.e. highlight
    // - Press SHFT + CTRL + X to execute code block

    import scalismo.geometry._
    import scalismo.common._
    import scalismo.ui.api._
    import scalismo.registration._
    import scalismo.kernels._
    import scalismo.statisticalmodel._
    import scalismo.io.StatisticalModelIO
    import breeze.linalg.DenseVector
    import utilities.methods._

    scalismo.initialize()
    implicit val rng = scalismo.utils.Random(42)
    val ui = ScalismoUI()

    // Load SSM
    val ssm = StatisticalModelIO.readStatisticalMeshModel(new java.io.File("C:/Working/Projects/Shoulder/Shape_models/scapula_normal_8scans.h5")).get
    val ssmGroup = ui.createGroup("ssmGroup")
    val ssmView = ui.show(ssmGroup, ssm, "ssm")

    val referenceMesh = ssm.referenceMesh
    val modelGroup = ui.createGroup("model")
    val refMeshView = ui.show(modelGroup, referenceMesh, "referenceMesh")

    // Load a target mesh to fit
    val targetMeshNotAligned = readOBJMesh("C:/Working/Projects/Shoulder/CT_segmentation/08-02078R/08-02078R Segmentation Shrinkwrap.obj")
    //val targetMeshNotAligned = readOBJMesh("C:/Working/Projects/Shoulder/CT_segmentation/16-02030R/16-02030R Segmentation Shrinkwrap.obj")
    val targetGroup = ui.createGroup("targetGroup")
    ui.show(targetGroup, targetMeshNotAligned, "target")

    // Code to get point ids from landmarks (for rigid 3D landmark registration)
    //val landmarkViews = ui.filter[LandmarkView](targetGroup, (v : LandmarkView) => true)
    //val landmarkIds : Seq[Int] = landmarkViews.map(lmView => {
    //  val lm = lmView.landmark
    //  val pid = targetMeshNotAligned.pointSet.findClosestPoint(lm.point).id
    //  pid.id
    //})

    // Aligned target mesh to reference mesh of ssm
    val pointIdsModel = IndexedSeq(862, 30783, 13062, 18890, 7940, 17484, 2131, 19785, 7053, 8169, 19666)                   // Manually picked - baseMesh
    val pointIdsTarget = IndexedSeq(388661, 902631, 1073939, 433779, 187844, 21571, 192530, 702326, 755957, 611697, 139183) // Manually picked - 08-02078R
    //val pointIdsTarget = IndexedSeq(896591, 760791, 282416, 948925, 863223, 41830, 2135, 794717, 930281, 560978, 538936)  // Manually picked - 16-02030R
    val correspondences = (0 until pointIdsModel.size).map(i => {
      val pnt_source = targetMeshNotAligned.pointSet.point(PointId(pointIdsTarget(i)))
      val pnt_target = referenceMesh.pointSet.point(PointId(pointIdsModel(i)))
      (pnt_source, pnt_target)
    })

    val rigidTrans = LandmarkRegistration.rigid3DLandmarkRegistration(correspondences, center = Point(0,0,0))
    val targetMesh = targetMeshNotAligned.transform(rigidTrans)
    val alignedGroup = ui.createGroup("Aligned")
    ui.show(alignedGroup, targetMesh, "target_aligned")

    // Show landmarks on referenceMesh and targetMesh
    val referenceLandmarks = pointIdsModel.map(id => Landmark(s"LM-${id}",referenceMesh.pointSet.point(PointId(id))))
    val referenceLandmarkViews = referenceLandmarks.map(lm => ui.show(modelGroup, lm, lm.id))
    val targetLandmarks = pointIdsTarget.map(id => Landmark(s"LM-${id}", targetMesh.pointSet.point(PointId(id))))
    val targetLandmarkViews = targetLandmarks.map(lm => ui.show(alignedGroup, lm, lm.id))

    // Augment ssm kernel to give the model more flexibility
    val scalarValuedKernel = GaussianKernel[_3D](30) * 5.0
    val matrixValuedKernel = DiagonalKernel(scalarValuedKernel, 3) // Non-symmetric kernel
    val GP = GaussianProcess[_3D, EuclideanVector[_3D]](matrixValuedKernel)
    val lowRankGP = LowRankGaussianProcess.approximateGPCholesky(
      referenceMesh.pointSet, GP, 0.05, NearestNeighborInterpolator())
    val ssmAugmented = StatisticalMeshModel.augmentModel(ssm, lowRankGP) // DiscreteLowRankGaussianProcess (i.e. not continuous)
    StatisticalModelIO.writeStatisticalMeshModel(ssmAugmented, new java.io.File("C:/Working/Projects/Shoulder/Shape_models/scapula_normal_8scans_augmented-30-5.h5"))

    // Create a continuous (not discrete) low rank gaussian process
    val interpolator = NearestNeighborInterpolator[_3D, EuclideanVector[_3D]]()
    val contGP = ssmAugmented.gp.interpolate(interpolator)

    // Transform all models within the modelGroup by the Gaussian process. This will be dynamically updated
    // during the registration process
    val gpView = ui.addTransformation(modelGroup, contGP, "gp")

    val registrationParameters = Seq(
      RegistrationParameters(regularizationWeight = 1e-1, numberOfIterations = 20, numberOfSampledPoints = 4000),
      RegistrationParameters(regularizationWeight = 1e-2, numberOfIterations = 20, numberOfSampledPoints = 8000),
      RegistrationParameters(regularizationWeight = 1e-4, numberOfIterations = 30, numberOfSampledPoints = 12000),
      RegistrationParameters(regularizationWeight = 1e-6, numberOfIterations = 50, numberOfSampledPoints = 16000)
    )

    // Set initial coefficients to zero i.e mean face
    val initialCoefficients = DenseVector.zeros[Double](contGP.rank)

    // Run the non-rigid registration
    val finalCoefficients = registrationParameters.foldLeft(initialCoefficients)((modelCoefficients, regParameters) =>
      doRegistration(contGP, referenceMesh, targetMesh, gpView, regParameters, modelCoefficients))

    // Create a new group containing the fitted mesh
    val resultGroup = ui.createGroup("Fitted_meshes")
    val transformationSpace = GaussianProcessTransformationSpace(contGP)
    val registrationTransformation = transformationSpace.transformForParameters(finalCoefficients)
    val fittedMesh = referenceMesh.transform(registrationTransformation)
    ui.show(resultGroup, fittedMesh, "fittedMesh")

    // Get a measure of the fit
    val AvgDist = scalismo.mesh.MeshMetrics.avgDistance(targetMesh, fittedMesh)
    val HsdrfDist = scalismo.mesh.MeshMetrics.hausdorffDistance(targetMesh, fittedMesh) // Can be large if necks are different lengths
    println(s"Average distance is $AvgDist")
    println(s"Hausdorff distance is $HsdrfDist")

    // Write aligned target mesh and fitted mesh to file
    writeQuadMeshToOBJ("fittedMesh", fittedMesh, basemeshName)
    writeTriangleMeshToOBJ("targetMesh_aligned", targetMesh)
}
