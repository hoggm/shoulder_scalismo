package utilities {

  import _root_.vtk._
  import scalismo.geometry._
  import scalismo.common._
  import scalismo.ui.api._
  import scalismo.mesh._
  import scalismo.kernels._
  import scalismo.registration._
  import scalismo.numerics._
  import scalismo.statisticalmodel._
  import breeze.linalg.DenseVector

  package object methods {

    implicit val rng = scalismo.utils.Random(42)
    val basemeshName : String = "C:/Working/Projects/Shoulder/scapula_quads_wrapped.obj"

    def readOBJMesh(filename : String) : TriangleMesh[_3D] = {

      // Function to read OBJ file containing quad elements only and return a TriangleMesh

      val objReader = new vtkOBJReader()
      objReader.SetFileName(filename)
      objReader.Update()
      val objModel = objReader.GetOutput()

      // Triangulate polyData (quad to tri mesh)
      val triFilter = new vtkTriangleFilter()
      triFilter.SetInputData(objModel)
      triFilter.Update()
      val triPolyData = triFilter.GetOutput()

      // Get points
      val numPoints : Int = triPolyData.GetNumberOfPoints()
      val points : IndexedSeq[Point[_3D]]= (0 until numPoints).map{i : Int =>
        val pc = triPolyData.GetPoint(i).toIndexedSeq.map{v:Double => v.toFloat}
        Point(pc(0),pc(1),pc(2))
      }

      // Get cells
      val numCells : Int = triPolyData.GetNumberOfCells()
      val cells : IndexedSeq[TriangleCell] = (0 until numCells).map{i : Int =>
        val pidList = triPolyData.GetCell(i).GetPointIds()
        val pids = (0 until pidList.GetNumberOfIds()).map{j:Int => PointId(pidList.GetId(j))}.toArray
        TriangleCell(pids(0),pids(1),pids(2))
      }

      // Create and return TriangleMesh
      val domain : UnstructuredPointsDomain[_3D] = UnstructuredPointsDomain(points)
      val triangles : TriangleList = TriangleList(cells)
      TriangleMesh3D(domain, triangles)
    }

    def TriangleToQuadMesh(triMesh : TriangleMesh[_3D], basemeshName : String) : vtkPolyData = {

      // Function to convert a TriangleMesh to a quad based mesh and write an OBJ file
      // Element connectivity for quad mesh is based on file "basemeshName"

      // Read basemesh to get number of points and quad connectivity
      val objReader = new vtkOBJReader()
      objReader.SetFileName(basemeshName)
      objReader.Update()
      val baseMesh = objReader.GetOutput()
      val numPoints : Int = baseMesh.GetNumberOfPoints()
      val numCells : Int = baseMesh.GetNumberOfCells()
      val quads = new vtkCellArray()
      (0 until numCells).foreach{i:Int =>
        val quad = new vtkQuad()
        val conn = baseMesh.GetCell(i).GetPointIds()
        (0 until conn.GetNumberOfIds()).foreach{j:Int => quad.GetPointIds().SetId(j,conn.GetId(j))}
        quads.InsertNextCell(quad)
      }

      // Read triMesh to get point coordinates.
      // Note that due to vtk vertex splitting in areas of high surface gradients to achieve better rendering,
      // the triMesh may have more points than the base mesh i.e. some points are duplicated due to splitting,
      // although the coordinates are identical. These duplicate nodes can be ignored. They don't have to be
      // merged,
      val points = new vtkPoints()
      (0 until numPoints).foreach{i:Int =>
        val p : Point[_3D] = triMesh.pointSet.point(PointId(i))
        points.InsertNextPoint(p(0),p(1),p(2))
      }

      // Create new polyData object consisting of point coordinates from triMesh and cell data from baseMesh
      val polyData = new vtkPolyData()
      polyData.SetPoints(points)
      polyData.SetPolys(quads)
      // Calculate points normals
      val normals = new vtkPolyDataNormals()
      normals.SetInputData(polyData)
      normals.ComputePointNormalsOn()
      normals.ComputeCellNormalsOff()
      normals.SplittingOff()
      normals.Update()
      normals.GetOutput()
    }

    def writeQuadMeshToOBJ(objFilePrefix : String, triMesh : TriangleMesh[_3D], basemeshName : String) : Unit = {
      val polydata = TriangleToQuadMesh(triMesh : TriangleMesh[_3D], basemeshName : String)
      writeOBJMesh(objFilePrefix, polydata)
    }

    def writeTriangleMeshToOBJ(objFilePrefix : String, triMesh : TriangleMesh[_3D]) : Unit = {
      // Points
      val points = new vtkPoints()
      triMesh.pointSet.points.foreach{p : Point[_3D] =>
        points.InsertNextPoint(p(0),p(1),p(2))
      }
      // Cells
      val cells = new vtkCellArray()
      (0 until triMesh.cells.size).foreach(i => {
        val tri = new vtkTriangle()
        (0 until 3).foreach(j => tri.GetPointIds().SetId(j, triMesh.cells(i).pointIds(j).id))
        cells.InsertNextCell(tri)
      })
      // Create polydata
      val polydata = new vtkPolyData()
      polydata.SetPoints(points)
      polydata.SetPolys(cells)
      // Calculate points normals
      val normals = new vtkPolyDataNormals()
      normals.SetInputData(polydata)
      normals.ComputePointNormalsOn()
      normals.ComputeCellNormalsOff()
      normals.SplittingOff()
      normals.Update()
      // Write wavefront (obj and mtl) files
      writeOBJMesh(objFilePrefix, normals.GetOutput(), true)
    }

    def writeOBJMesh(objFilePrefix : String, polydata: vtkPolyData, clean : Boolean = false) : Unit = {
      // Clean polydata - Merge duplicate points. This is required because vtk splits the mesh
      // in areas of high surface gradients to achieve better rendering
      // NOTE: cleanPolyData renumbers the vertices, which is an issue for shape modelling where
      //       point correspondence is required
      var cleaned = polydata
      if (clean == true) {
        val cleanPolydata = new vtkCleanPolyData()
        cleanPolydata.SetInputData(polydata)
        cleanPolydata.Update()
        cleaned = cleanPolydata.GetOutput()
      }

      // Write wavefront (obj and mtl) files. Conversion to obj requires rendering of polyData in vtkRenderWindow
      val objWriter = new vtkOBJExporter()
      objWriter.SetFilePrefix(objFilePrefix)
      val renderer = new vtkRenderer()
      val dataMapper = new vtkPolyDataMapper()
      dataMapper.SetInputData(cleaned)
      val actor = new vtkActor()
      actor.SetMapper(dataMapper)
      renderer.AddActor(actor)
      val renWin = new vtkRenderWindow()
      renWin.AddRenderer(renderer)
      objWriter.SetInput(renWin)
      objWriter.Write()
    }

    case class XmirroredKernel(kernel : PDKernel[_3D]) extends PDKernel[_3D] {
      override def domain = RealSpace[_3D]
      override def k(x: Point[_3D], y: Point[_3D]) = kernel(Point(x(0) * -1f ,x(1), x(2)), y)
    }

    def symmetrizeKernel(kernel : PDKernel[_3D]) : MatrixValuedPDKernel[_3D] = {
      val xmirrored = XmirroredKernel(kernel)
      val k1 = DiagonalKernel(kernel, 3)
      val k2 = DiagonalKernel(xmirrored * -1f, xmirrored, xmirrored)
      k1 + k2
    }

    // Define class to do non-rigid registration
    case class RegistrationParameters(regularizationWeight : Double, numberOfIterations : Int, numberOfSampledPoints : Int)
    def doRegistration(
                        lowRankGP : LowRankGaussianProcess[_3D, EuclideanVector[_3D]],  // Can this be discrete, or must be continuous?
                        referenceMesh : TriangleMesh[_3D],
                        targetmesh : TriangleMesh[_3D],
                        gpView: ShowInScene.CreateLowRankGPTransformation.View,
                        registrationParameters : RegistrationParameters,
                        initialCoefficients : DenseVector[Double]
                      ) : DenseVector[Double] =
    {
      val transformationSpace = GaussianProcessTransformationSpace(lowRankGP)
      val fixedImage = referenceMesh.operations.toDistanceImage
      val movingImage = targetmesh.operations.toDistanceImage
      val sampler = FixedPointsUniformMeshSampler3D(
        referenceMesh,
        registrationParameters.numberOfSampledPoints
      )
      val metric = MeanSquaresMetric(
        fixedImage,
        movingImage,
        transformationSpace,
        sampler
      )
      val optimizer = LBFGSOptimizer(registrationParameters.numberOfIterations)
      val regularizer = L2Regularizer(transformationSpace)
      val registration = Registration(
        metric,
        regularizer,
        registrationParameters.regularizationWeight,
        optimizer
      )
      val registrationIterator = registration.iterator(initialCoefficients)
      val visualizingRegistrationIterator = for ((it, itnum) <- registrationIterator.zipWithIndex) yield {
        println(s"object value in iteration $itnum is ${it.value}")
        gpView.coefficients = it.parameters
        it
      }
      val registrationResult = visualizingRegistrationIterator.toSeq.last
      registrationResult.parameters
    }

  }}
