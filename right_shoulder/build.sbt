name := "right_shoulder"

version := "0.1"

scalaVersion := "2.12.10"

resolvers += Resolver.bintrayRepo("unibas-gravis", "maven")

libraryDependencies ++=
  Seq("ch.unibas.cs.gravis" %% "scalismo-ui" % "0.13.1",
    "ch.unibas.cs.gravis" %% "scalismo" % "0.17.2")
